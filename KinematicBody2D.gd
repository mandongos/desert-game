extends KinematicBody2D

signal collided

export var gravity = 2500
export var speed = 300
export var jump = -1200

var jumping = false
var velocity = Vector2()

func get_input():
	velocity.x = 0
	jumping = false
	if Input.is_action_pressed('ui_right'):
		velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= 1
	velocity.x *= speed
	if Input.is_action_pressed('ui_select'):
		jumping = true
		if is_on_floor() and jumping:
			velocity.y = jump +100
	
	$AnimatedSprite.play()
	
	if velocity.x != 0 and is_on_floor():
			$AnimatedSprite.animation = 'Right'
			$AnimatedSprite.flip_v = false
			$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y <=0:
			$AnimatedSprite.animation = 'Jump'
	else:
		$AnimatedSprite.animation = 'Idle'

func _physics_process(delta):
	velocity.y += gravity * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision:
			emit_signal('collided', collision)
	if position.y > 650:
			get_tree().change_scene("res://Hud.tscn")

	if is_on_floor() and jumping:
		velocity.y = jump