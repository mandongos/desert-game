extends KinematicBody2D


var run_speed = 200
var velocity = Vector2.ZERO
var player = null

func _physics_process(delta):
	velocity = Vector2.ZERO
	if player:
		velocity = (player.position - position).normalized() * run_speed
	velocity = move_and_slide(velocity)




func _on_Area2D_body_entered(body):
	player = body
	
func _on_Area2D_body_exited(body):
	player = null
